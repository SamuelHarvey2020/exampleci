echo "Running unit tests with coverage stats"
coverage run --omit */tests_*,*__init__.py -m unittest discover -s project -v
if [ $? -ne 0 ]; then
	echo "Unit test failed"
	exit 1
fi


